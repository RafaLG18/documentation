# Docker

Nesse tópico, você aprenderá como a preparar o ambiente e instalar o docker em seu host linux.

## Docker Engine 
### Preparando o ambiente
 Desinstale versões mais antigas através do comando:

* `sudo apt-get remove docker docker-engine docker.io containerd runc`

Execute para atualizar os pacotes e instalar outros pacotes importantes para instalação:

* `apt-get update`

* ```
  sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
 ```

Adicione chave GPG oficial:

* `curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -`

Verifique se você possui a chave através do comando fingerprint:

* `sudo apt-key fingerprint 0EBFCD88`

Tal comando deverá retornar isso na tela:

* `pub   rsa4096 2017-02-22 [SCEA]
       9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88
 uid           [ unknown] Docker Release (CE deb) <docker@docker.com>
 sub   rsa4096 2017-02-22 [S]`

Adicione o repósitório com o seguinte comando:
 
* `sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable" `

 Importante lembrar que é bom mudar esse `$(lsb_release -cs)`  para a distribuição do seu linux, por exemplo, se você usa ubuntu, o seu código ficará asim:

* `sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   bionic \
   stable"`

### Instalando docker

Com o ambiente configurado e pronto, podemos agora instalar o docker-engine com o seguinte comando:

* ` sudo apt-get update && sudo apt-get install docker-ce docker-ce-cli containerd.io`

### Finalização

Agora, para finalizar e testar se a instalação foi correta, vamos executar um pequeno container com o seguinte comando:

* `sudo docker run hello-world`

## Docker-compose 
### Instalação

Incialmente, a instalação do docker-compose consiste em realizar o download da versão estável de seu arquivo, e conceder permissões ao mesmo através dos seguintes comandos: 

- Download:

`sudo curl -L "https://github.com/docker/compose/releases/download/1.26.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose`  

- Permissões:

`chmod +x /usr/local/bin/docker-compose` 
### Finalização

Para finalizar a parte do docker-compose, vamos ver se o mesmo está funcionando. Execute o seguinte comando:

* `docker-compose --version`

Se não tiver qualquer mensagem de erro, então está tudo certo!

## Docker-machine
### Instalação

Sendo um opcional para instalar, o docker-machine pode ser instalado fazendo o download de seu binário e extraindo para seu PATH através desse comando:

* `base=https://github.com/docker/machine/releases/download/v0.16.0 &&
curl -L $base/docker-machine-$(uname -s)-$(uname -m) >/tmp/docker-machine &&
sudo mv /tmp/docker-machine /usr/local/bin/docker-machine &&
chmod +x /usr/local/bin/docker-machine`

### Finalização

Para ver se o docker-machine está funcionando correntamente, executaremos o seguinte comando:

* `docker-machine version`

Se não for retornado qualquer mensagem de erro, considere a instalação finalizada.

# Referências de instalação:
* [Docker engine](https://docs.docker.com/engine/install/ubuntu/)

* [Docker compose](https://docs.docker.com/compose/install/)

* [Docker machine](https://docs.docker.com/machine/install-machine/)
