# Para que serve?

Kind serve para que você crie um cluster localmente, agora, utilizando containers como nodes. Dessa forma, fica mais fácil o estudo do kubernetes e mais econômico.

# Instalação

## Pré-requisitos

Para você instalar o kind, você precisa do [Docker](/docker/) instalado, e o GO.