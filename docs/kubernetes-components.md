# Kubernetes compoments

Nesse tópico, você aprenderá sobre os componentes do kubernetes.

## Introdução 
![Boneca](./img/Boneca.png)

Matriosca, ou boneca russa, é um tradicional brinquedo russo. Constitui-se de uma série de bonecas, feitas geralmente de madeira, colocadas umas dentro das outras, da maior até a menor. 

Quando se estuda componentes kubernetes, podemos compará-los à bonecas russas, pois seu funcionamento se assemelha à níveis, ou camadas, e cada nível possui uma função diferente, e estes estão correlacionados um com outro.

## Primeiro nível

Primeiro nível, ou mais alto (se podemos chamar assim), consiste no Cluster(s).

### Cluster

![Componentes](./img/components.png)

Como mostrado na imagem acima, o *Cluster* é a parte do kubernetes, no qual são encontrados os _Nodes_ e o *Control plane*.

Inicialmente, a função do cluster é executar os nodes e gerenciá-los.

## Segundo nível

Como foi dito anteriormente, os *Nodes* e o *Control Plane* compõem o *Cluster*. Por causa disso, ficam no segundo nível.

### Node

![Nodes](./img/Nodes.png)

Inialmente, no node pode ser encontrado os pods (representado pelos computadores acima).

O node é responsável por executar os pods e gerenciá-los. E o este gerencialmento ocorre por meio de três ferramentas: *Container runtime*, *Kubelet* e *Kube-proxy*.

### Control Plane 

![ControlPlane](./img/CPlane.png)

Entretanto, o Control Plane é reponsável por realizar as decisões globais no cluster. E, ele é a parte do cluster que gerencia os nodes.

Seu gerenciamento se dá por meio de outras ferramentas: *Kube-apiserver*,*Etcd*,*Kube-scheduler*,*Kube-controller-manager*
, e *Cloud-controller-manager*, sendo que estes dois últimos, são formados por outros componentes, os quais serão abordados mais para frente.

## Terceiro nível

Terceiro nível, ou mais baixo (se podemos chamar assim), consiste nos componentes do *Node* e *Control Plane*.

### Componentes node

#### Kubelet
 
 Kubelet é um agente que é executado em cada node de um cluster, ele garante que os containers estejam sendo executados dentro do Pod. Essa ferramenta pega um conjunto de PodSpecs que são fornecidos através de vários mecanismos e garantem que os containers descritos em seus PodSpecs estejam executando e saudáveis.

#### Kube-Proxy

 É a rede proxy que roda em cada node do cluster, implementando parte do conceito de serviço kubenetes. Além disso, Essa ferramenta mantém as regras de rede, as quais permitem comunicação de rede entra os pods.

#### Container runtime

 É o software responsável por executar os containers.

### Componentes Control Plane

#### Kube-apiserver

Inicialmente, ele expõe o Kubernetes API, além disso, serve como front-end para o control plane

#### Etcd

Inicialmente, ele serve como armazenamento de valores-chave usado como para armazenamento de apoio do kubernetes para todos os dados do cluster. Em outras palavras, serve como banco de dados.

#### Kube Scheduler 

Ele serve para ver pods,sem nodes, criados recentemente e seleciona um node para relocá-lo.

#### Kube-controller-manager

Executa controladores de processos:

 **Node controller :**
 Responsável por notar e responder quando um node cai.
 
 **Replication controller:**
 Responsável por manter o número correto de pods para cada objeto do replication controller no sistema.
 
 **Endpoints controller:**
 Preenche o objeto de Endpoint, ou seja, junta-se serviços aos pods.

#### Cloud-controller-manager

Permite vincular cluster ao API do provedor de nuvem, e separa componentes que interagem com o cloud, dos que apenas interagem com o cluster.
Componentes:

 **Node Controller:**
 Verifica provedor cloud para determinar se o node foi deletado da nuvem depois de para de responder.

 **Route Controller:**
 Configura rotas na infraestrutura da nuvem subjacente.

 **Service Controller:**
 Cria, atualiza e deleta load balancers do provedor cloud.
 
