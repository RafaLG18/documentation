# Kubernetes 

Aqui você aprenderá a instalar e configurar o kubernetes em sua máquina linux.

## Instalação

### Instalando dependências

Para depois instalar o kubernetes, é necessário instalar o [Docker](/docker/) e executar o seguinte comando:

* `sudo systemctl start docker && sudo systemctl enable docker`

### Instalando kubernetes

Baixe e adicione a chave para instalação kubernetes com o seguinte comando: 

* `sudo curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add `

Crie e Abre o arquivo com o seguinte comando:

* `vim /etc/apt/sources.list.d/kubernetes.list`

Depois copie e cole o seguinte 
repositório no arquivo:

* `deb http://apt.kubernetes.io/ kubernetes-xenial main`  

Salve e feche o arquivo. Agora instale o kubernetes com o seguinte comando:

* `sudo apt-get update && sudo apt-get install -y kubelet kubeadm kubectl kubernetes-cni`

### Disabilite o swap

Para conseguir executar o kubernetes, é necessário desabilitar o swap com o seguinte comando:

* `sudo swapoff -a`

Deixe a mudança permanente:

* `sudo vim /etc/fstab/`

No arquivo, comente o swap adicionando # 

* `/dev/mapper/vgubuntu-swap_1 none   swap sw 0  0`

## Configuração

### Inicialize o master

 Agora que já está tudo instalado, vamos configurar o host que você instalou o k8s para ser o master do cluster.

Incialmente execute o seguite comando:

* `sudo kubeadm init`

 *OBS*: Quando o comando terminar de executar salve o comando final que ele der, será muito importante para adicionar outras máquinas no seu cluster.Comando que está sendo referido:

* `kubeadm join IP-HOST:6443 --token uvoidq.sud82suu7af2utax \    --discovery-token-ca-cert-hash sha256:139a2efc506e1d5a1fe5c2cd9f860d1c3f81319bce0b4c9553a283bb54a439a4`

Esse IP_HOST será o IP do seu host master, daí toda maquina que você executar esse comando, será acrescentada como mais um nó no seu cluster.

## Referências para a instalação e configuração

* [Kubernetes](https://www.techrepublic.com/article/how-to-quickly-install-kubernetes-on-ubuntu/)